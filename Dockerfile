FROM node:8.3

WORKDIR /code
COPY ["src", "/code"]

RUN npm install && \
    npm run buildStatic
CMD [ "npm", "start" ]

