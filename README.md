# Shereland Web

Web is the microservice for requests for the website. It is supposed to substitute the legacy site.

## Running

```command
docker-compose up -d
```

Open [localhost:3000/blog/](http://localhost:3000/blog/).

## Testing

```command
docker-compose run --rm web npm test
```
