/*
 * This script should not be runned.
 * The purpose of it is to be source for a repl
 * 
 * Start with:
 * npm run provider
 */

var pacts = require('./test/pacts.js');
const provider = pacts.providerGraphql;
provider.setup();
setTimeout(() => {pacts.addInteraction('all', provider)}, 2000);
