module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    browserify: {
      dist: {
        options: {
           transform: [
             ['babelify', {presets: ['es2015', 'react']}]
           ]
        },
        src: ['public/react/book-suggestions.js'],
        dest: 'public/react-generated/book-suggestions.js',
      },
      contact: {
        options: {
           transform: [
             ['babelify', {presets: ['es2015', 'react']}]
           ]
        },
        src: ['public/react/contact.js'],
        dest: 'public/react-generated/contact.js',
      }
    },
    concat: {
      base: {
        src: [
          'public/javascripts/jquery.js',
          'public/javascripts/bootstrap.js',
          'public/javascripts/sweetalert.min.js',
          'public/javascripts/sign.js',
          'public/javascripts/jquery-ui.min.js',
          'public/javascripts/search.js',
          'public/javascripts/magnific-popup.js',
          'public/javascripts/newsletter.js',
          'public/javascripts/how-it-works.js'
        ],
        dest: 'build/javascripts/base.js',
      },
      blog: {
        src: [
          'public/javascripts/social.js',
          'public/javascripts/blog.js'
        ],
        dest: 'build/javascripts/blog.js',
      },
      book_suggestions: {
        src: [
          'public/react-generated/book-suggestions.js',
        ],
        dest: 'build/javascripts/book-suggestions.js',
      },
      contact: {
        src: [
          'public/react-generated/contact.js',
        ],
        dest: 'build/javascripts/contact.js',
      },
    },
    cssmin: {
      target: {
        files: {
          'build/stylesheets/base.css': [
            'public/stylesheets/font-awesome-min.css',
            'public/stylesheets/bootstrap.css',
            'public/stylesheets/jquery-ui.min.css',
            'public/stylesheets/style.css',
            'public/stylesheets/shereland.css',
          ],
          'build/stylesheets/blog.css': [
            'public/stylesheets/magnific-popup.css',
            'public/stylesheets/blog.css',
          ],
          'build/stylesheets/semantic-ui-fix.css': [
            'public/stylesheets/semantic-ui-fix.css',
          ]
        }
      }
    },
    uglify: {
      static_mappings: {
        files: [
          {src: 'build/javascripts/base.js', dest: 'build/javascripts/base.js'},
          {src: 'build/javascripts/blog.js', dest: 'build/javascripts/blog.js'},
          {src: 'build/javascripts/book-suggestions.js', dest: 'build/javascripts/book-suggestions.js'},
          {src: 'build/javascripts/contact.js', dest: 'build/javascripts/contact.js'},
        ],
      },
    },
    hash: {
      javascripts: {
        options: {
          mapping: 'mapping/static.json',
          srcBasePath: 'build/',
          destBasePath: 'dist/',
        },
        src: 'build/javascripts/*.js',
        dest: 'dist/javascripts/',
      },
      stylesheets: {
        options: {
          mapping: 'mapping/static.json',
          srcBasePath: 'build/',
          destBasePath: 'dist/',
        },
        src: 'build/stylesheets/*.css',
        dest: 'dist/stylesheets/'
      },
    },
    copy: {
      fontawesome: {
        files: [
          {
            src: [
              'stylesheets/fonts/**',
            ],
            dest: 'dist/',
            cwd: 'public/',
            expand: true,
          },
          {
            src: ['stylesheets/font-awesome-min.css'],
            dest: 'dist/',
            cwd: 'public/',
            expand: true,
          }
        ]
      },
      images: {
        files: [
          {
            src: ['images/**'],
            dest: 'dist/',
            cwd: 'public/',
            expand: true,
          }
        ]
      }
    },
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-hash');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-browserify');

  // Default task(s).
  grunt.registerTask('default', ['browserify', 'concat', 'cssmin', 'uglify', 'hash', 'copy']);

};
