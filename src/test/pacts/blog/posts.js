const graphqlQueries = require('../../../graphqlQueries')
    , mocks = require('../../mocks/blog')
    , mocksBase = require('../../mocks/base');

const interactionPosts = {
  state: 'there are posts',
  uponReceiving: 'request for posts',
  withRequest: {
    method: 'POST',
    path: '/graphql',
    headers: { 'Content-Type': 'application/json' },
    body: {
      query: graphqlQueries.posts,
      variables: {
        page: 1,
        category: null,
      }
    }
  },
  willRespondWith: {
    status: 200,
    headers: { 'Content-Type': 'application/json' },
    body: {
      data: {
        notifications: mocksBase.notificationsLogged,
        latestTweets: mocksBase.latestTweets,
        posts: {
          currentPage: 1,
          lastPage: 10,
          data: mocks.mockedPosts,
        },
        popularPosts: mocks.mockedPopularPosts,
      }
    }
  }
}

const interactionPostsPage2 = {
  state: 'there are posts',
  uponReceiving: 'request for posts on page 2',
  withRequest: {
    method: 'POST',
    path: '/graphql',
    headers: { 'Content-Type': 'application/json' },
    body: {
      query: graphqlQueries.posts,
      variables: {
        page: 2,
        category: null,
      }
    }
  },
  willRespondWith: {
    status: 200,
    headers: { 'Content-Type': 'application/json' },
    body: {
      data: {
        notifications: mocksBase.notificationsLogged,
        latestTweets: mocksBase.latestTweets,
        posts: {
          currentPage: 1,
          lastPage: 10,
          data: mocks.mockedPostsPage2,
        },
        popularPosts: mocks.mockedPopularPosts,
      }
    }
  }
}

module.exports = {
  interactionPosts,
  interactionPostsPage2,
}
