const graphqlQueries = require('../../../graphqlQueries')
    , mocks = require('../../mocks/blog');

const interactionPosts = {
  state: 'there are posts',
  uponReceiving: 'request for category posts',
  withRequest: {
    method: 'POST',
    path: '/graphql',
    headers: { 'Content-Type': 'application/json' },
    body: {
      query: graphqlQueries.posts,
      variables: {
        category: 'acontece',
        page: 1,
      }
    }
  },
  willRespondWith: {
    status: 200,
    headers: { 'Content-Type': 'application/json' },
    body: {
      data: {
        notifications: {
          friendshipRequests: [
            {
              slug: 'walter-isaacson',
              name: 'Walter Isaacson'
            }
          ]
        },
        latestTweets: [
          {
            content: 'Melhor notícia: último livro da tetralogia de Elena Ferrante chega às livrarias no fim do mês http://bit.ly/2oQv47D  #ferrantefever'
          },
          {
            content: '10 fotos (maravilhosas) de Marilyn Monroe perdida nos livros: http://bit.ly/2g7KejE '
          }
        ],
        posts: {
          currentPage: 1,
          lastPage: 10,
          data: mocks.mockedPosts,
        },
        popularPosts: mocks.mockedPopularPosts,
      }
    }
  }
}

module.exports = {
  interactionPosts,
}
