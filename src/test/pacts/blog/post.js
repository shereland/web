const graphqlQueries = require('../../../graphqlQueries')
, mocks = require('../../mocks/blog');

const interactionPost = {
  state: 'there are posts',
  uponReceiving: 'request for single post page',
  withRequest: {
    method: 'POST',
    path: '/graphql',
    headers: { 'Content-Type': 'application/json' },
    body: {
      query: graphqlQueries.post,
      variables: {
        post: 'this-is-my-first-post',
      }
    }
  },
  willRespondWith: {
    status: 200,
    headers: { 'Content-Type': 'application/json' },
    body: {
      data: {
        notifications: {
          friendshipRequests: [
            {
              slug: 'walter-isaacson',
              name: 'Walter Isaacson'
            }
          ]
        },
        latestTweets: [
          {
            content: 'Melhor notícia: último livro da tetralogia de Elena Ferrante chega às livrarias no fim do mês http://bit.ly/2oQv47D  #ferrantefever'
          },
          {
            content: '10 fotos (maravilhosas) de Marilyn Monroe perdida nos livros: http://bit.ly/2g7KejE '
          }
        ],
        post: mocks.mockedFullPost,
        popularPosts: mocks.mockedPopularPosts,
      }
    }
  }
}

const interactionPostNotFound = {
  state: 'there are no posts',
  uponReceiving: 'request for 404 post page',
  withRequest: {
    method: 'POST',
    path: '/graphql',
    headers: { 'Content-Type': 'application/json' },
    body: {
      query: graphqlQueries.post,
      variables: {
        post: '404-post',
      }
    }
  },
  willRespondWith: {
    status: 200,
    headers: { 'Content-Type': 'application/json' },
    body: {
      data: {
        notifications: {
          friendshipRequests: [
            {
              slug: 'walter-isaacson',
              name: 'Walter Isaacson'
            }
          ]
        },
        latestTweets: [
          {
            content: 'Melhor notícia: último livro da tetralogia de Elena Ferrante chega às livrarias no fim do mês http://bit.ly/2oQv47D  #ferrantefever'
          },
          {
            content: '10 fotos (maravilhosas) de Marilyn Monroe perdida nos livros: http://bit.ly/2g7KejE '
          }
        ],
        post: null,
        popularPosts: mocks.mockedPopularPosts,
      }
    }
  }
}

module.exports = {
  interactionPost,
  interactionPostNotFound,
}
