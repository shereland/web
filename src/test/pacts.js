const graphqlQueries = require('../graphqlQueries')
    , mocks = require('./mocks/blog')
    , pact = require('pact')
    , pactBlogPost = require('./pacts/blog/post')
    , pactBlogPosts = require('./pacts/blog/posts')
    , pactBlogCategory = require('./pacts/blog/category')
    , path = require('path');
    
const BFFGRAPHQL_PORT = parseInt(process.env.BFFGRAPHQL_PORT);

const handleDone = (done) => {
  if (typeof done != 'undefined') done();
}

pacts = {
  providerGraphql: pact({
    consumer: 'web',
    provider: 'bff-graphql',
    port: BFFGRAPHQL_PORT,
    log: path.resolve(process.cwd(), 'logs', 'pact.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    logLevel: 'INFO',
    spec: 2
  }),

  /*
   * Single scope is used for tests, which use to require only one response.
   * Case of all is for to be used as a mockup server, useful for
   * debbuging with `npm start`.
   */
  addInteraction(scope, provider, done) {
    if (scope == 'posts home' || scope == 'all') {
      provider.addInteraction(pactBlogPosts.interactionPosts).then(() => {
        handleDone(done);
      });
    }
    if (scope == 'posts home only') {
      provider.addInteraction(pactBlogPosts.interactionPostsOnly).then(() => {
        handleDone(done);
      });
    }
    if (scope == 'posts page 2' || scope == 'all') {
      provider.addInteraction(pactBlogPosts.interactionPostsPage2).then(() => {
        handleDone(done);
      });
    }
    if (scope == 'category' || scope == 'all') {
      provider.addInteraction(pactBlogCategory.interactionPosts).then(() => {
        handleDone(done);
      });
    }
    if (scope == 'post' || scope == 'all') {
      provider.addInteraction(pactBlogPost.interactionPost).then(() => {
        handleDone(done);
      });
    }
    if (scope == 'post not found' || scope == 'all') {
      provider.addInteraction(pactBlogPost.interactionPostNotFound).then(() => {
        handleDone(done);
      });
    }
  }
}

module.exports = pacts;
