const mockedFullPost = {
  slug: 'this-is-my-first-post',
  title: 'This is my first post',
  category: 'Acontece',
  categorySlug: 'acontece',
  content: '<p>Content of my first post. This accept html. This will contain all data!</p>',
  publishedAt: '1502064376',
  url: '/blog/livros/acontece/this-is-my-first-post',
  author: {
    username: 'gabriela'
  },
  comments: [
    {
      name: 'Ada Lovelace',
      site: 'https://en.wikipedia.org/wiki/Ada_Lovelace',
      message: 'Really liked this post :)',
      createdAt: '1502928741'
    },
    {
      name: 'Alan Turing',
      site: 'https://en.wikipedia.org/wiki/Alan_Turing',
      message: 'I agree. This is good!',
      createdAt: '1502928749'
    }
  ],
  relatedBooks: [
    {
      title: 'The Innovators',
      link: 'https://www.shereland.com/livros/os-inovadores'
    },
    {
      title: 'Steve Jobs',
      link: 'https://www.shereland.com/livros/steve-jobs'
    }
  ],
  relatedPosts: [
    {
      title: '10 dicas para reciclar seus livros velhos',
      link: 'https://www.shereland.com/blog/livros/rodape/dicas-reciclar-livros-velhos'
    },
    {
      title: 'Novidade: o que é que o Kindle Oasis tem de bom?',
      link: 'https://www.shereland.com/blog/livros/acontece/o-que-kindle-oasis-tem-de-bom'
    },
  ],
  album: {
    title: 'Fundos de tela de livros famosos',
    photos: [
      {
        thumbnail: 'https://d269syrwt0zgsa.cloudfront.net/media/cache/09/6c/096cbf6d45d09e4486a7589b54ceaef8.jpg',
        image: 'https://d269syrwt0zgsa.cloudfront.net/media/album-photos/harry-potter-wallpaper-hogwarts-place.jpg',
        title: 'Castelo de Hogwarts',
        description: 'Castelo de Hogwarts com boa foto: http://pichost.me/1655031/'
      },
      {
        thumbnail: 'https://d269syrwt0zgsa.cloudfront.net/media/cache/0d/dc/0ddcdffeaf09fb222384dd9754902a6d.jpg',
        image: 'https://d269syrwt0zgsa.cloudfront.net/media/album-photos/the-lord-of-the-rings-gate-of-argonath.jpg',
        title: 'Portão de Agornath',
        description: 'Ainda no primeiro filme, mas com esse belo portão: http://www.desktopwallpapers4.me/movies/gate-of-argonath-lord-of-the-rings-13245/'
      },
    ]
  }
};

const mockedPosts = [
  mockedFullPost,
  {
    slug: 'this-is-my-second-post',
    title: 'This is my second post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/a-gente-le/this-is-my-second-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  },
  {
    slug: 'this-is-my-third-post',
    title: 'This is my third post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/rodape/this-is-my-third-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  },
  {
    slug: 'this-is-my-fourth-post',
    title: 'This is my fourth post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/acontece/this-is-my-third-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  }
];

const mockedPostsPage2 = [
  {
    slug: 'this-is-my-fifth-post',
    title: 'This is my fifth post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/a-gente-le/this-is-my-fifth-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  },
  {
    slug: 'this-is-my-sixth-post',
    title: 'This is my sixth post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/rodape/this-is-my-sixth-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  },
  {
    slug: 'this-is-my-seventh-post',
    title: 'This is my seventh post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/acontece/this-is-my-third-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  },
  {
    slug: 'this-is-my-eighth-post',
    title: 'This is my eighth post',
    category: 'Acontece',
    categorySlug: 'acontece',
    content: '<p>I am writing another nice post. This will content only the required fields!</p>',
    publishedAt: '1502064676',
    url: '/blog/livros/acontece/this-is-my-third-post',
    author: {
      username: 'gabriela'
    },
    relatedBooks: [],
    relatedPosts: [],
    album: {
      title: null,
      photos: []
    }
  },
];

const mockedPopularPosts = [
  {
    title: 'Conheça a linda história entre Carlos Drummond de Andrade e a filha, Maria Julieta',
    url: 'https://www.shereland.com/blog/livros/acontece/drummond-e-maria-julieta'
  },
  {
    title: 'Por que a tetralogia de Elena Ferrante é irresistível?',
    url: 'https://www.shereland.com/blog/livros/a-gente-le/resenha-elena-ferrante'
  },
  {
    title: 'Frases e versos de Matilde Campilho em Jóquei',
    url: 'https://www.shereland.com/blog/livros/rodape/frases-e-versos-de-matilde-campilho'
  },
  {
    title: 'Melhores citações de Chimamanda em Sejamos Todos Feministas + baixe o e-book gratuitamente',
    url: 'https://www.shereland.com/blog/livros/acontece/citacoes-de-chimamanda-em-sejamos-todos-feministas'
  },
  {
    title: 'A gente lê: Antologia da Literatura Fantástica',
    url: 'https://www.shereland.com/blog/livros/a-gente-le/antologia-da-literatura-fantastica'
  },
];

module.exports = {
  mockedFullPost,
  mockedPopularPosts,
  mockedPosts,
  mockedPostsPage2,
}
