
const latestTweets = [
  {
    content: 'Melhor notícia: último livro da tetralogia de Elena Ferrante chega às livrarias no fim do mês http://bit.ly/2oQv47D  #ferrantefever'
  },
  {
    content: '10 fotos (maravilhosas) de Marilyn Monroe perdida nos livros: http://bit.ly/2g7KejE '
  }
]

const notificationsLogged = {
  friendshipRequests: [
    {
      slug: 'walter-isaacson',
      name: 'Walter Isaacson'
    }
  ]
}

module.exports = {
  latestTweets,
  notificationsLogged,
}
