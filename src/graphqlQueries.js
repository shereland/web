const queries = {
  post: `
    query($post: String!) {
      notifications {
        friendshipRequests {
          slug
          name
        }
      }
      latestTweets {
        content
      }
      post(slug: $post) {
        slug
        title
        content
        category
        categorySlug
        publishedAt
        url
        author {
          username
        }
        comments {
          createdAt
          name
          site
          message
        }
        relatedBooks {
          title
          link
        }
        relatedPosts {
          title
          url
        }
        album {
          title
          photos {
            thumbnail
            image
            title
            description
          }
        }
      }
      popularPosts {
        title
        url
      }
    }
  `,
  posts: `
    query($page: Int, $category: String) {
      notifications {
        friendshipRequests {
          slug
          name
        }
      }
      latestTweets {
        content
      }
      posts(page: $page, category: $category) {
        currentPage
        lastPage
        data {
          slug
          title
          content
          publishedAt
          url
          author {
            username
          }
          comments {
            createdAt
            name
            site
            message
          }
          relatedBooks {
            title
            link
          }
          relatedPosts {
            title
            url
          }
          album {
            title
            photos {
              thumbnail
              image
              title
              description
            }
          }
        }
      }
      popularPosts {
        title
        url
      }
    }
  `
}

module.exports = queries;
