const app = require('./app')
    , redis = require("redis")
    , client = redis.createClient({'host': 'redis_db'})
    , chai = require('chai')
    , chaiAsPromised = require('chai-as-promised')
    , request = require('supertest');
chai.use(chaiAsPromised);

describe('Application', () => {

  it('should redirect if exist a destination', () => {
    request(app)
      .get('/blog/not-existent-page')
      .expect(404)
      .end((err, res) => {
        if (err) throw err;
        done();
      });

    client.set('/blog/not-existent-page', (err, _) => {
      request(app)
      .get('/blog/not-existent-page')
      .expect(301)
      .end((err, res) => {
        if (err) throw err;
        done();
      });
    })
  })
});
