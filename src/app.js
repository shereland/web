const express = require('express')
    , path = require('path')
    , favicon = require('serve-favicon')
    , logger = require('morgan')
    , cookieParser = require('cookie-parser')
    , bodyParser = require('body-parser')
    , redis = require("redis")
    , client = redis.createClient({'host': 'redis_db'})
    , i18n = require('node-translate')
    , helpers = require('./helpers/views')
    , staticMapping = require('./mapping/static.json')
    , blog = require('./routes/blog')
    , book = require('./routes/book')
    , staticRoutes = require('./routes/static')
    , app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'dist')));

// Redis session
app.use(function(req, res, next) {
  if (req.cookies.sessionid) {
    client.get(req.cookies.sessionid, function(err, reply) {
      req.userSession = reply;
      next();
    });
  }
  else {
    next();
  }
});

// Authenticate user
app.use(function(req, res, next) {
  if (req.userSession) {
    var sessionData = new Buffer(req.userSession, 'base64').toString('ascii');
    var userData = sessionData.substr(sessionData.indexOf(':') + 1);
    req.user = JSON.parse(userData);
  }
  next()
});

// Get user locale
i18n.requireLocales({
  'en': require('./locales/en'),
  'pt-br': require('./locales/pt-br'),
});
app.use(function(req, res, next) {
  req.locale = 'pt-br';
  // TODO: Do not force pt-br locale
  i18n.setLocale(req.locale);
  req.i18n = i18n;
  next();
});

// Add stylesheets and javascript files required for all layouts
app.use(function(req, res, next) {
  req.requiredCssBefore = [
    staticMapping['stylesheets/base.css'],
  ];
  req.requiredCssAfter = [];
  req.requiredJs = [
    staticMapping['javascripts/base.js'],
  ];
  next();
});

app.use('/blog', blog);
app.use('/livros', book);
app.use('/contato', staticRoutes['contact']);
app.use('/quem-somos', staticRoutes['about-us']);
app.use('/faq', staticRoutes['faq']);
app.use('/politica-de-privacidade', staticRoutes['privacy-policy']);
app.use('/termos-de-uso', staticRoutes['terms-of-use']);

// redirects for search engines
// set in redis: `SET redirect:/blog/old-page /blog/new-page EX 3000000` (~1 month)
app.use(function(req, res, next) {
  client.get('redirect:' + req.url, (err, destination) => {
    if (destination) {
      res.redirect(301, destination);
    }
    else {
      next();
    }
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  console.error(err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', helpers(req));
});

module.exports = app;
