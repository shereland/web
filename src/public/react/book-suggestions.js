import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Popup } from 'semantic-ui-react'


class LoadingTopBar extends React.Component {

  render() {
    const style = {
      backgroundColor: 'white',
      color: '#16a085',
      position: 'fixed',
      zIndex: 10000,
      left: 0,
      top: 0,
      width: '100%',
      textAlign: 'center',
      height: '30px',
    }
    return this.props.isLoading ?
      <div style={style}>
        <p>
          Carregando
          <img src={window.staticHost + "images/loader.gif"} />
        </p>
      </div>
      :
      null;
  }
}

class BooksSuggestion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: null,
      old: [],
      bookFor: [],
      literatureType: {},
      suggestionResults: null,
      isLoading: false,
    }

    this.selectGender = this.selectGender.bind(this);
    this.selectOld = this.selectOld.bind(this);
    this.selectBookFor = this.selectBookFor.bind(this);
    this.changeLiteratureType = this.changeLiteratureType.bind(this);
    this.suggest = this.suggest.bind(this);
  }

  selectGender(e) {
    this.state["gender"] = e.target.value;
    this.suggest(e);
  }

  selectOld(e) {
    this.state.old = [];
    var options = e.target.options;

    for (var i = 0; i < options.length; i++) {
      if (options[i].selected) {
        this.state.old.push(options[i].value);
      }
    }

    this.suggest(e);
  }

  selectBookFor(e) {
    this.state.bookFor = [];
    var options = e.target.options;

    for (var i = 0; i < options.length; i++) {
      if (options[i].selected) {
        this.state.bookFor.push(options[i].value);
      }
    }

    this.suggest(e);
  }

  changeLiteratureType(e) {
    var target = e.target;
    var name = target.name;
    var value = target.value;
    this.state.literatureType[name] = value;
    this.suggest(e);
  }

  getParameters() {
    var state = this.state;
    var query = "?";

    if (state.gender == "masculino" || state.gender == "feminino") {
      query += "gender=" + state.gender + "&";
    }

    for (var i = 0; i < state.old.length; i++) {
      var item = state.old[i];
      query += "old=" + item + "&";
    }

    for (var i = 0; i < state.bookFor.length; i++) {
      var item = state.bookFor[i];
      query += "book_for=" + item + "&";
    }

    for (var key in state.literatureType) {
      var value = state.literatureType[key];
      query += key + "=" + value + "&";
    }

    return query;
  }

  suggest(e) {
    this.setState({ isLoading: true });

    fetch("https://www.shereland.com/api/books/suggestion" + this.getParameters())
      .then(function (response) {
        return response.json();
      })
      .then((jsonData) => {
        this.setState({
          isLoading: false,
          suggestionResults: jsonData.data.books,
        });
      }).catch((err) => {
        this.setState({ isLoading: false });
        console.error(err);
      })
  }

  render() {
    return (

      <div>
        <LoadingTopBar isLoading={this.state.isLoading} />

        <form method="POST" className="suggestion-form">

          <div className="form-group">
            <h3 htmlFor="gender">Gênero</h3>
            <select className="form-control" name="gender" onChange={this.selectGender}>
              <option value="none">Indiferente</option>
              <option value="masculino">Masculino</option>
              <option value="feminino">Feminino</option>
            </select>
          </div>

          <div className="form-group">
            <h3 htmlFor="gender">Faixa etária</h3>
            <p><em>(Apertar Ctrl e selecionar para escolher múltiplas Idades)</em></p>
            <select multiple className="form-control" name="old" onChange={this.selectOld}>
              <option value="0-a-2">0 a 2</option>
              <option value="3-a-5">3 a 5</option>
              <option value="6-a-8">6 a 8</option>
              <option value="9-a-12">9 a 12</option>
              <option value="13-a-16">13 a 16</option>
              <option value="17-a-21">17 a 21</option>
              <option value="22-a-30">22 a 30</option>
              <option value="31-a-40">31 a 40</option>
              <option value="41-a-5">41 a 50</option>
              <option value="51-em-diante">51 em diante</option>
            </select>
          </div>

          <div className="form-group">
            <h3 htmlFor="gender">Quero um livro para</h3>
            <p><em>(Apertar Ctrl e selecionar para escolher múltiplas opções)</em></p>
            <select multiple className="form-control" name="book_for" onChange={this.selectBookFor}>
              <option value="me-divertir">Me divertir</option>
              <option value="chorar">Chorar</option>
              <option value="me-distrair">Me distrair</option>
              <option value="suspirar-de-amor">Suspirar de amor</option>
              <option value="esquecer-um-amor">Esquecer um amor</option>
              <option value="pensar">Pensar</option>
              <option value="navegar-numa-estoria-bem-contada">Navegar numa estória bem contada</option>
              <option value="enxergar-a-realidade">Enxergar a realidade</option>
              <option value="ter-fe-na-humanidade">Ter fé na humanidade</option>
              <option value="descobrir-quem-sou-eu">Descobrir quem sou eu</option>
              <option value="aprender-a-gostar-de-ler">Aprender a gostar de ler</option>
              <option value="conhecer-mais-e-mais-e-mais">Conhecer mais e mais e mais</option>
              <option value="ler-no-busão">Ler no busão</option>
              <option value="um-dia-de-preguiça">Um dia de preguiça</option>
              <option value="ler-antes-de-dormir">Ler antes de dormir</option>
              <option value="ficar-rico">Ficar rico</option>
            </select>
          </div>

          <div className="form-group">
            <h3>Gosto literário</h3>
            <table className="table table-bordered text-center">
              <tbody>
                <tr>
                  <th className="text-center" width="50">Gosto</th>
                  <th className="text-center" width="90">Não gosto</th>
                  <th className="text-center" width="90">Indiferente</th>
                  <th></th>
                </tr>

                <tr>
                  <td><input type="radio" name="hq" onChange={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="hq" onChange={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="hq" onChange={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">HQ</td>
                </tr>

                <tr>
                  <td><input type="radio" name="bestseller" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="bestseller" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="bestseller" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Best-seller</td>
                </tr>

                <tr>
                  <td><input type="radio" name="baseadoemfatosreais" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="baseadoemfatosreais" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="baseadoemfatosreais" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Baseado em fatos reais</td>
                </tr>

                <tr>
                  <td><input type="radio" name="classico" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="classico" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="classico" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Clássico</td>
                </tr>

                <tr>
                  <td><input type="radio" name="poesia" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="poesia" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="poesia" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Poesia</td>
                </tr>

                <tr>
                  <td><input type="radio" name="fantasia" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="fantasia" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="fantasia" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Fantasia</td>
                </tr>

                <tr>
                  <td><input type="radio" name="ficcaocientifica" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="ficcaocientifica" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="ficcaocientifica" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Ficção científica</td>
                </tr>

                <tr>
                  <td><input type="radio" name="terror" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="terror" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="terror" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Terror</td>
                </tr>

                <tr>
                  <td><input type="radio" name="acao" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="acao" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="acao" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Ação</td>
                </tr>

                <tr>
                  <td><input type="radio" name="literaturanacional" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="literaturanacional" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="literaturanacional" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Literatura nacional</td>
                </tr>

                <tr>
                  <td><input type="radio" name="literaturaestrangeira" onClick={this.changeLiteratureType} value="1" /></td>
                  <td><input type="radio" name="literaturaestrangeira" onClick={this.changeLiteratureType} value="0" /></td>
                  <td><input type="radio" name="literaturaestrangeira" onClick={this.changeLiteratureType} value="" defaultChecked /></td>
                  <td className="text-left">Literatura estrangeira</td>
                </tr>

              </tbody>
            </table>
          </div>

        </form>
        <BooksResults
          results={this.state.suggestionResults}
          isLoading={this.state.isLoading}
          gender={this.state.gender}
          old={this.state.old}
          bookFor={this.state.bookFor}
          literatureType={this.state.literatureType}
        />
      </div>
    );
  }
}

class BooksResults extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      emailSent: false,
    }

    this.askBookSuggestions = this.askBookSuggestions.bind(this);
  }

  askBookSuggestions(e) {
    e.preventDefault();

    var body = 'Content:\n';
    body += "Email: " + e.target.email.value + '\n';
    body += "Sexo: " + this.props.gender + '\n';

    body += 'Idades: \n';
    for (var i = 0; i < this.props.old.length; i++) {
      var item = this.props.old[i];
      body += "- " + item + '\n';
    }

    body += "Livros para: \n";
    for (var i = 0; i < this.props.bookFor.length; i++) {
      var item = this.props.bookFor[i];
      body += "- " + item + '\n';
    }

    body += 'Gosto literário: \n';
    for (var key in this.props.literatureType) {
      var value = this.props.literatureType[key];
      if (value != null && value != "") {
        body += "- " + key + ": " + value + '\n';
      }
    }

    var data = new FormData();
    data.append("body", body);

    this.setState({ isLoading: true, })
    fetch("https://www.shereland.com/api/books/empty-tips", {
      method: "POST",
      body: data
    })
      .then((response) => {
        this.setState({
          isLoading: false,
          emailSent: true,
        });

        // Hide message after 10 seconds
        setTimeout(() => {
          this.setState({emailSent: false,})
        }, 10000);
      });
  }

  render() {
    if (this.props.isLoading) {
      return (<div>
        <h2>Buscando livros</h2>
        <img src={window.staticHost + "images/loader.gif"} />
      </div>)
    }

    if (this.props.results === null) {
      return null;
    }

    if (this.props.results.length === 0) {
      return <div>
        <h1>Sugestões não encontradas!</h1>
        <p>
          Estamos trabalhando para sempre colocar mais e mais recomendações de livros.
          Se estiver interessado, preencha seu e-mail abaixo para que pensemos em
          alguns livros que possa achar interessante!
        </p>

        <form className="form-inline clearfix" onSubmit={this.askBookSuggestions}>
          <div className="form-group">
            <input className="form-control" id="id_email" name="email" placeholder="E-mail" type="text" />
          </div>
          <button type="submit" className="btn btn-primary">Enviar</button>
        </form>

        {this.state.isLoading &&
          <img src={window.staticHost + "images/loader.gif"} />
        }

        <p>&nbsp;</p>

        {this.state.emailSent &&
          <div>
            <h1>Pedido enviado</h1>
            <p>
              Assim que possível, retornaremos para o e-mail preenchido com sugestões de leituras.
            </p>
          </div>
        }

      </div>;
    }

    return (
      <div className="row">
        <div className="col-sm-12"><h1>Sugestões de Livros</h1></div>
        {this.props.results.map((book) => { return <BookResult key={book.slug} book={book} />; })}
      </div>
    )
  }
}

class BookResult extends React.Component {

  render() {
    const book = this.props.book;
    var tags = [];
    for (var key in book.related_suggestions) {
      tags.push(book.related_suggestions[key]['name'])
    }
    const tags_list = tags.join(' - ');

    return <div className="col-sm-4">
      <p>
        <a target="_blank" className="link-portfolio no-margin" href={book.url}>
          <img className="img-responsive img-home-portfolio" src={book.thumbnail} alt={book.title} />
        </a>
        <Popup trigger={<Button>Ver tags</Button>} content={tags_list} />
      </p>
      <br />
    </div>;
  }
}

ReactDOM.render(
  <BooksSuggestion />,
  document.getElementById('root_book_suggestions')
);
