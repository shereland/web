import React from 'react';
import ReactDOM from 'react-dom';
import gql from "graphql-tag";
import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: "https://graphql.shereland.com/graphql"
});


class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      subject: "Dúvidas",
      how_found_site: "Amigos",
      message: "",
      readonly: false,
    }

    this.changeName = this.changeName.bind(this);
    this.changeEmail = this.changeEmail.bind(this);
    this.changePhone = this.changePhone.bind(this);
    this.changeSubject = this.changeSubject.bind(this);
    this.changeHowFoundSite = this.changeHowFoundSite.bind(this);
    this.changeMessage = this.changeMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
  }

  changeName(e) {
    this.setState({"name": e.target.value});
  }

  changeEmail(e) {
    this.setState({"email": e.target.value})
  }

  changePhone(e) {
    this.setState({"phone": e.target.value})
  }

  changeSubject(e) {
    this.setState({"subject": e.target.value})
  }

  changeHowFoundSite(e) {
    this.setState({"how_found_site": e.target.value})
  }

  changeMessage(e) {
    this.setState({"message": e.target.value})
  }

  sendMessage(e) {
    e.preventDefault();
    this.setState({readonly: true});

    client
      .mutate({
        mutation: gql`
                  mutation($contactInput: ContactInput!) {
                    contact(contactInput: $contactInput) {
                      error
                    }
                  }
        `,
        variables: {
          "contactInput": {
            "name": this.state.name,
            "email": this.state.email,
            "phone": this.state.phone,
            "subject": this.state.subject,
            "how_found_site": this.state.how_found_site,
            "message": this.state.message,
          }
        },
      })
      .then(result => {
        // TODO: Add alert if worked
        // TODO: Show error if didn't work
        this.setState({readonly: false});
        this.setState({
          name: "",
          email: "",
          phone: "",
          message: "",
        });
      });
  }

  render() {
    return (<div className="row">
      <div className="col-sm-8">
        <h3>Contato</h3>
        <p>Entre em contato com os responsáveis pelo site.</p>

        <form role="form" method="POST" action="/contato" onSubmit={this.sendMessage}>
          <div className="row">

            <div className="form-group col-md-12">
              <label htmlFor="input1">Nome</label>
              <input type="text" name="name" id="id_name" required="" placeholder="Nome" className="form-control" maxLength="100" value={this.state.name} onChange={this.changeName} readOnly={this.state.readonly} />
            </div>

            <div className="form-group col-md-12">
              <label htmlFor="input1">E-mail</label>
              <input type="text" name="email" required="" placeholder="E-mail" className="form-control" id="id_email" value={this.state.email} onChange={this.changeEmail} readOnly={this.state.readonly} />
            </div>

            <div className="form-group col-md-12">
              <label htmlFor="input1">Telefone</label>
              <input type="text" name="phone" id="id_phone" placeholder="Telefone ou celular (opcional)" className="form-control" maxLength="20" value={this.state.phone} onChange={this.changePhone} readOnly={this.state.readonly} />
            </div>

            <div className="form-group col-md-12">
              <label htmlFor="input1">Assunto</label>
              <select name="subject" className="form-control" id="id_subject" onChange={this.changeSubject} disabled={this.state.readonly}>
                <option value="Dúvidas">Dúvidas</option>
                <option value="Sugestões">Sugestões</option>
                <option value="Outros">Outros</option>
              </select>
            </div>

            <div className="form-group col-md-12">
              <label htmlFor="input1">Como achou o site</label>
              <select name="how_found_site" className="form-control" id="id_how_found_site" onChange={this.changeHowFoundSite} disabled={this.state.readonly}>
                <option value="Amigos">Amigos</option>
                <option value="Facebook">Facebook</option>
                <option value="Bing">Bing</option>
                <option value="DuckDuckGo">DuckDuckGo</option>
                <option value="Google">Google</option>
                <option value="Yahoo">Yahoo</option>
                <option value="Outras ferramentas de busca">Outras ferramentas de busca</option>
                <option value="Twitter">Twitter</option>
                <option value="Outros">Outros</option>
              </select>
            </div>

            <div className="form-group col-md-12">
              <label htmlFor="input1">Mensagem</label>
              <textarea name="message" rows="10" id="id_message" required="" placeholder="Mensagem" cols="40" className="form-control" value={this.state.message} onChange={this.changeMessage} readOnly={this.state.readonly} />
             </div>

            <div className="form-group col-lg-12">
              <input type="hidden" name="save" value="contact" />
              <button type="submit" className="btn btn-primary pull-right" disabled={this.state.readonly}>Submit</button>
              <div className="clearfix"></div>
            </div>

          </div>
        </form>
      </div>

      <div className="col-sm-4">
        <h3>Localização</h3>
        <h4>São Paulo</h4>
        <p><i className="fa fa-envelope"></i> <a href="mailto:contato@shereland.com">contato@shereland.com</a></p>
      </div>

    </div>
    );
  }
}

ReactDOM.render(
  <Contact />,
  document.getElementById('root_contact')
);
