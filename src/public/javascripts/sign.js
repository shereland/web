$(document).ready(function () {

  // Login with username/password

  $(".form-login").submit(function () {
    var data = $(this).serialize();
    var form = this;
    $.post("/api/user/login", data)
      .done(function (response) {
        if (response.result == 'success') {
          if (response.redirect) {
            window.location = response.redirect;
          }
          else if (window.location.pathname == '/')
            window.location = "/dashboard";
          else
            location.reload();
        }
        else {
          sweetAlert("Erro", response.message, "error");
        }
      })
      .fail(function () {
        sweetAlert("Erro", response.message, "Houve alguma falha no sistema. Por favor, tente novamente. Se o erro persistir, por favor, entre em contato.");
      });
    return false;
  });

  // Login/Signup with Facebook

  $('.facebook-sign').click(function () {
    FB.login(function (response) {
      if (response.authResponse) {
        data = FB.getAuthResponse();
        $.post("/api/user/signup-facebook", data)
          .done(function (response) {
            if (response.result == 'success') {

              // Redirect to finish sign up with all possible data
              if (response.type == 'login') {
                if (window.location.pathname == '/')
                  window.location = "/dashboard";
                else
                  location.reload();
              }
              else {
                window.location = response.redirect;
              }
            }
            else {
              sweetAlert("Erro", response.message, "error");
            }
          })
          .fail(function () {
            sweetAlert("Erro", response.message, "Houve alguma falha no sistema. Por favor, tente novamente. Se o erro persistir, por favor, entre em contato.");
          });
      } else {
        sweetAlert("Erro", 'Você confirmou as permissões necessárias?', "error");
      }
    }, { scope: 'email,user_location,user_friends' });
  });
});
