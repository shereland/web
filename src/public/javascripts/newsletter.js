function send_newsletter_mail(target) {                                                                                                                                                                   
    var email = $(target).val();                                                                                                                                                                          
    $.get("/api/newsletter", {email:email})                                                                                                                                                               
        .done(function(data){                                                                                                                                                                             
            if (data.result == 'success')                                                                                                                                                                 
                sweetAlert("", 'Obrigado! Seu e-mail foi cadastrado com sucesso, e em breve começará a receber nossos e-mails.', "success");                                                              
            else if (data.result == 'fail') {                                                                                                                                                             
                sweetAlert("Erro", data.errors.email, "error");                                                                                                                                           
            }                                                                                                                                                                                             
        })                                                                                                                                                                                                
        .fail(function(){
            sweetAlert("Erro", "Houve alguma falha no sistema. Por favor, tente novamente. Se o erro persistir, por favor, entre em contato.", "error");                                                  
        });                                                                                                                                                                                               
}           
$('#newsletter-button').click(function(){send_newsletter_mail('#newsletter-text');});                                                                                                                     
$("#newsletter-text").keyup(function (e) {if (e.keyCode == 13) {send_newsletter_mail('#newsletter-text');}});                                                                                             
$('#sidebar-newsletter-button').click(function(){send_newsletter_mail('#sidebar-newsletter-text');});                                                                                                     
$("#sidebar-newsletter-text").keyup(function (e) {if (e.keyCode == 13) {send_newsletter_mail('#sidebar-newsletter-text');}});                                                                             
