$(document).ready(function () {

  $(".form-comment").submit(function () {
    var data = $(this).serialize();
    var form = this;
    $.post("/api/comment", data)
      .done(function (response) {
        if (response.result == 'success') {

          // Get values for jQuery response

          var id = form.post.value;
          var name = form.name.value;
          var content = form.content.value;
          var rand_target = "scroll-" + (1 + Math.floor(Math.random() * 10000));
          var message = '<div class="comment-content pull-left" id="' + rand_target + '">';
          message += '<h4>' + name + ' <small>Alguns segundos atrás</small></h4>';
          message += '<p>' + content + '</p>';
          message += '</div>'

          // Insert for user

          $('#comments-' + id).after(message);

          // Blank form information

          form.name.value = '';
          form.email.value = '';
          form.site.value = '';
          form.content.value = '';

          // Tell user everything is ok

          sweetAlert("Obrigado!", "Seu comentário foi enviado. ", "success");

          // Scroll to comment

          var target = $('#' + rand_target);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: (target.offset().top - 56)
            }, 1000);
            return false;
          }
        }
        else {
          sweetAlert("Erro", 'Houve algum erro no preenchimento dos dados.', "error");
        }
      })
      .fail(function () {
        sweetAlert("Erro", "Houve alguma falha no sistema. Por favor, tente novamente. Se o erro persistir, por favor, entre em contato.", "error");
      });
    return false;
  });

});
