$(document).ready(function () {

  fn_add_new_book = function (data) {
    add = confirm('Cadastrar novo livro chamado "' + data['title'] + '"?');

    if (add) {
      $.post("/api/books/add_new_book_to_shelf", data)
        .done(function (response) {
          if (response.result == 'success') {
            if (undefined != response.redirect)
              window.location = response.redirect
            else
              location.reload();
          }
          else {
            sweetAlert("Erro", response.message, "error");
          }
        });
    }
    return false;
  }

  $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        var li;
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        li = that._renderItemData( ul, item );
        if ( item.category ) {
          li.attr( "aria-label", item.category + " : " + item.label );
        }
      });
    }
  });
  $( "#autocomplete" ).catcomplete({
    delay: 400,
    source: 'https://www.shereland.com/api/search',
    select: function (event, ui) {
      action = ui.item.action;
      title = ui.item.label;
      if (action == 'link') {
        window.location = ui.item.url;
      }
      else if (action == 'add-book') {
        data = { 'title': title }
        fn_add_new_book(data);
      }
    },
  });

});
