const express = require('express')
  , helpers = require('../helpers/views')
  , router = express.Router()
  , staticMapping = require('../mapping/static.json');


/*
 * Books suggestions are rendered using React
 * User can search books to read
 */
router.get('/dicas', function (req, res, next) {
  req.requiredCssBefore.unshift('//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css');
  req.requiredCssAfter.push(staticMapping['stylesheets/semantic-ui-fix.css']);
  req.requiredJs.push(staticMapping['javascripts/book-suggestions.js']);
  res.render('book-suggestions', helpers(req))
});

module.exports = router;
