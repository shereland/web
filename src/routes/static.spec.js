const app = require('../app')
    , redis = require("redis")
    , client = redis.createClient({'host': 'redis_db'})
    , chai = require('chai')
    , chaiAsPromised = require('chai-as-promised')
    , request = require('supertest');
chai.use(chaiAsPromised);
var assert = require('assert');

describe('Static pages', () => {

  it('should render contact', (done) => {
    request(app)
      .get('/contato')
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        done();
      });
  });

  it('should render about us', (done) => {
    request(app)
      .get('/quem-somos')
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        done();
      });
  });

  it('should render faq', (done) => {
    request(app)
      .get('/faq')
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        done();
      });
  });

  it('should render privacy policies', (done) => {
    request(app)
      .get('/politica-de-privacidade')
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        done();
      });
  });

  it('should render terms of use', (done) => {
    request(app)
      .get('/termos-de-uso')
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        done();
      });
  });
});
