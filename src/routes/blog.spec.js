const app = require('../app')
    , chai = require('chai')
    , chaiAsPromised = require('chai-as-promised')
    , pacts = require('../test/pacts')
    , request = require('supertest');
chai.use(chaiAsPromised);

const provider = pacts.providerGraphql;

before(() => provider.setup());
after(() => provider.finalize());

describe('Blog', function() {

  describe('all posts', function() {

    before(done => pacts.addInteraction('posts home', provider, done));
    before(done => pacts.addInteraction('posts page 2', provider, done));

    it('should render all posts', function(done) {
      this.timeout(5000)
      request(app)
      .get('/blog/')
      .expect('Content-Type', /html/)
      .expect(200)
      .end((err, res) => {
        var content = res.text;
        if (!(content.indexOf('This is my first post') >= 0)) throw new Error('First post not appearing!');
        if (!(content.indexOf('This is my second post') >= 0)) throw new Error('Second post not appearing!');

        // The header and footer should be tested by index.js
        // But for now, there is no index.js, so will be tested here
        // TODO: Make the header and footer test at index.js
        if (!(content.indexOf('<a class="navbar-brand" href="/">Shereland</a>') >= 0))
          throw new Error('Header title not appearing!');
        if (!(content.indexOf('<a class="footer-brand" href="/">Shereland</a>') >= 0))
          throw new Error('Footer title not appearing!');
        done();
      })
    });

    it('should render page 2', function(done) {
      this.timeout(5000)
      request(app)
      .get('/blog/?page=2')
      .expect('Content-Type', /html/)
      .expect(200)
      .end((err, res) => {
        var content = res.text;
        if (!(content.indexOf('This is my fifth post') >= 0)) throw new Error('Fifth post not appearing!');
        if (!(content.indexOf('This is my sixth post') >= 0)) throw new Error('Sixth post not appearing!');
        if (!(content.indexOf('This is my seventh post') >= 0)) throw new Error('Seventh post not appearing!');
        if (!(content.indexOf('This is my eighth post') >= 0)) throw new Error('Eighth post not appearing!');

        done();
      })
    });

    it('successfully verifies', () => provider.verify());

  });

  describe('posts by category', function() {

    before(done => pacts.addInteraction('category', provider, done));

    it('should render posts', function(done) {
      this.timeout(5000)

      request(app)
      .get('/blog/livros/acontece/')
      .expect('Content-Type', /html/)
      .expect(200)
      .end((err, res) => {
        var content = res.text;
        if (!(content.indexOf('This is my first post') >= 0)) throw new Error('First post on category not appearing!');
        if (!(content.indexOf('This is my second post') >= 0)) throw new Error('Second post on category not appearing!');

        done();
      })
    })
    
    it('successfully verifies', () => provider.verify());
    
  })
  
  describe('single post page', function() {
    
    before(done => pacts.addInteraction('post', provider, done));
    before(done => pacts.addInteraction('post not found', provider, done));

    it('should render a single post', function(done) {
      this.timeout(5000)

      request(app)
      .get('/blog/livros/acontece/this-is-my-first-post')
      .expect('Content-Type', /html/)
      .expect(200)
      .end((err, res) => {
        var content = res.text;
        if (!(content.indexOf('This is my first post') >= 0)) throw new Error('First post on single post not appearing!');
        if ((content.indexOf('This is my second post') >= 0)) throw new Error('Second post on single post appearing!');
        
        done();
      })
    })

    it('should render 404 for post not found', function(done) {
      this.timeout(5000)

      request(app)
      .get('/blog/livros/acontece/404-post')
      .expect('Content-Type', /html/)
      .expect(404)
      .end((err, res) => {
        var content = res.text;
        if ((content.indexOf('This is my first post') >= 0)) throw new Error('First post on single post not appearing!');
        if ((content.indexOf('This is my second post') >= 0)) throw new Error('Second post on single post appearing!');
        
        done();
      })
    })

    it('successfully verifies', () => provider.verify());
  })
});
