const express = require('express')
  , helpers = require('../helpers/views')
  , getGraphqlClient = require('../helpers/graphql').getGraphqlClient
  , graphqlQueries = require('../graphqlQueries')
  , pagination = require('../helpers/routes').pagination
  , pactHelpers = require('../helpers/pact')
  , router = express.Router()
  , request = require('request')
  , staticMapping = require('../mapping/static.json');

/*
 * The host is build by hostname and port.
 * It is separated, so it is just change ports for testing and mocking.
*/
const BFFGRAPHQL_PORT = parseInt(process.env.BFFGRAPHQL_PORT)
  , BFFGRAPHQL_HOSTNAME = process.env.BFFGRAPHQL_HOST
  , BFFGRAPHQL_HOST = BFFGRAPHQL_HOSTNAME + ':' + BFFGRAPHQL_PORT;

/*
 * There are /blog/ and /blog/:category/ that uses the same render logic.
 * So, the renderPostsPage will be the same method for both.
 */
const renderPostsPage = (req, res, options) => {
  var data = options.data;
  // TODO: Only add library if is rendering a post with album
  req.requiredCssAfter.push(staticMapping['stylesheets/blog.css']);
  req.requiredJs.push(staticMapping['javascripts/blog.js']);

  res.render('posts', Object.assign(
    {
      headTitle: options['headTitle'],
      title: options['title'],
      category: options['category'],
      nFriendsRequests: data.notifications.friendshipRequests.length,
      posts: data.posts.data,
      popularPosts: data.popularPosts,
      latestTweets: data.latestTweets,
      pagination: pagination(data.posts.currentPage, data.posts.lastPage),
    },
    // Helpers for all pages, like static function
    helpers(req)
  ))
}

/*
 * This route should render all posts.
 * First page of the blog.
 */
router.get('/', function (req, res, next) {
  var variables = {
    page: parseInt(req.query.page || '1'),
    category: null,
  }

  var client = getGraphqlClient(req);
  client.request(graphqlQueries.posts, variables)

    .then((data) => {
      renderPostsPage(req, res, {
        headTitle: 'Shereland - Blog',
        title: 'Shereland - Blog',
        data: data,
      })
    })
    .catch((err) => {
      console.log(err)
      next(err)
    });
});

/*
 * This route should render just a category.
 */
router.get('/livros/:category/', function (req, res, next) {
  var category = req.params.category;
  var categoryName = {
    'acontece': req.i18n.t('Happens'),
    'desafio': req.i18n.t('Challenge'),
    'no-rodape': req.i18n.t('In the footer'),
    'a-gente-le': req.i18n.t('We read'),
    'eles-leem': req.i18n.t('They read'),
  }[category];
  var variables = {
    page: parseInt(req.query.page || '1'),
    category: category,
  }

  var client = getGraphqlClient(req);
  client.request(graphqlQueries.posts, variables)
    .then((data) => {
      renderPostsPage(req, res, {
        headTitle: 'Shereland - Blog - ' + categoryName,
        title: categoryName,
        category: categoryName,
        data: data,
      })
    })
    .catch((err) => {
      console.log(err)
      next(err)
    });
});

/*
 * This route is for a specific post.
 */
router.get('/livros/:category/:post', function (req, res, next) {
  var category = req.params.category;
  var post = req.params.post;
  var variables = {
    post: post,
  }

  var client = getGraphqlClient(req);
  client.request(graphqlQueries.post, variables)
    .then((data) => {
      // TODO: Only add library if is rendering a post with album
      req.requiredCssAfter.push(staticMapping['stylesheets/blog.css']);
      req.requiredJs.push(staticMapping['javascripts/blog.js']);

      if (data.post == null) {
        next();
        return;
      }

      var post = data.post;
      post.ldjson = JSON.stringify({
        "@context": "http://schema.org",
        "@type": "BlogPosting",
        "headline": post.title,
        "genre": post.category,
        "url": "https://www.shereland.com/blog/livros/" + post.categorySlug + '/' + post.slug,
        "datePublished": (new Date(parseInt(post.publishedAt + '000'))).toISOString(),
        "articleBody": post.content.replace(/<\/?[^>]+(>|$)/g, ""),
        "author": {
          "type": "http://schema.org/Person",
          "name": post.author.username,
	},
        "publisher": {
          "type": "http://schema.org/Person",
          "name": post.author.username,
	}
      })

      res.render('post', Object.assign(
        {
          headTitle: data.post.title + " | Shereland",
          title: data.post.title,
          nFriendsRequests: data.notifications.friendshipRequests.length,
          post: post,
          popularPosts: data.popularPosts,
          latestTweets: data.latestTweets,
        },
        // Helpers for all pages, like static function
        helpers(req)
      ))
    })
    .catch((err) => {
      console.log(err)
      next(err)
    });
});

module.exports = router;
