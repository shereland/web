const express = require('express')
  , helpers = require('../helpers/views')
  , staticMapping = require('../mapping/static.json');

const staticRoutes = {
  'about-us': function(req, res, next) {
    res.render('static/about-us', helpers(req))
  },
  'contact': function(req, res, next) {
    req.requiredJs.push(staticMapping['javascripts/contact.js']);
    res.render('static/contact', helpers(req))
  },
  'faq': function(req, res, next) {
    res.render('static/faq', helpers(req))
  },
  'privacy-policy': function(req, res, next) {
    res.render('static/privacy-policy', helpers(req))
  },
  'terms-of-use': function(req, res, next) {
    res.render('static/terms-of-use', helpers(req))
  },
} 

module.exports = staticRoutes;
