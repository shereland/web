const GraphQLClient = require('graphql-request').GraphQLClient
  , BFFGRAPHQL_PORT = parseInt(process.env.BFFGRAPHQL_PORT)
  , BFFGRAPHQL_HOSTNAME = process.env.BFFGRAPHQL_HOST
  , BFFGRAPHQL_HOST = BFFGRAPHQL_HOSTNAME + ':' + BFFGRAPHQL_PORT;

/*
 * It should be different for each request, because have to retrieve different
 * sessionsID (or JTW token in the future)
 */
function getGraphqlClient(req) {
  var headers = {}
  if (typeof req.cookies.sessionid != 'undefined')
    headers['sessionid'] = req.cookies.sessionid;
  return new GraphQLClient(BFFGRAPHQL_HOST + '/graphql', {
    headers: headers,
  })
}

module.exports = { getGraphqlClient }
