const assert = require('assert')
    , helpers = require('../helpers/routes')
    , pagination = helpers.pagination;

describe('Routes Helpers', function() {

  describe('pagination', function() {

    it('should show previous on pages > 1', function() {
      var p;
      p = pagination(1, 10);
      assert.equal(p.previousPage, null);
      p = pagination(2, 10);
      assert.equal(p.previousPage, 1);
    });

    it('should show next on pages < last', function() {
      var p;
      p = pagination(1, 10);
      assert.equal(p.nextPage, 2);
      p = pagination(10, 10);
      assert.equal(p.nextPage, null);
    });

  });

});
