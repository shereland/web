const moment = require('moment')
    , urlize = require('../helpers/urlize');

/*
 * The host is build by hostname and port.
 * It is separated, so it is just change ports for testing and mocking.
*/
const BFFGRAPHQL_PORT = parseInt(process.env.BFFGRAPHQL_PORT)
  , BFFGRAPHQL_HOSTNAME = process.env.BFFGRAPHQL_HOST
  , BFFGRAPHQL_HOST = BFFGRAPHQL_HOSTNAME + ':' + BFFGRAPHQL_PORT;


helpers = function(req) {
  return {
    graphqlHost: BFFGRAPHQL_HOST,

    requiredCssBefore: req.requiredCssBefore,
    requiredCssAfter: req.requiredCssAfter,
    requiredJs: req.requiredJs,

    static: function(filename) {
      if (filename.match(/^(http[s])|(\/\/)/)) {
        return filename;
      }
      else if (process.env.NODE_ENV == 'production')
        return process.env.STATIC_URL + '/' + filename;
      return '/' + filename;
    },

    user: req.user,
    username: function() {
      if (req.user) {
        return req.user.username;
      }
      return null;
    }(),

    urlize: function(text) {
      return urlize(text, {
        nofollow: true,
        target: '_blank',
        trim: 'http',
      })
    },

    newLineToBr: function(text) {
      return text.replace("\n", "<br />");
    },

    pluralize: function(obj, empty, singular, plural) {
      var number;
      if (obj == null) number = 0;
      else if (typeof obj === 'object') number = obj.length;
      else number = obj;

      if (number == 0) return empty;
      if (number == 1) return '1 ' + singular;
      return number + ' ' + plural;
    },

    stripTags: function(text) {
      return text.replace(/<\/?[^>]+(>|$)/g, "");
    },

    humanizeDate: function(text) {
      moment.locale(req.locale);
      var d = new Date(parseInt(text) * 1000);
      return moment(d).format('LLL');
    },

    // Simple translation method
    _: function(text) {
      return req.i18n.t(text)
    }
  }
}

module.exports = helpers;
