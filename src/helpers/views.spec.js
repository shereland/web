const assert = require('assert')
    , helpers = require('../helpers/views')

describe('Views Helpers', function() {

  describe('static', function() {

    it('should keep link for development', function() {
      var cdnStatic = helpers({}).static('static.file');
      assert.equal(cdnStatic, '/static.file', 'Static not working for development');
    });

    it('should add cdn link for production', function() {
      this.timeout(5000);

      // Prepare environment for simulating production environment
      var env = process.env;
      process.env = {
        NODE_ENV: 'production',
        STATIC_URL: 'https://mycnd.com'
      }

      var cdnStatic = helpers({}).static('static.file');
      assert.equal(cdnStatic, 'https://mycnd.com/static.file', 'Static not working for production');

      process.env = env;
    });

    it('should keep absolute URLs', () => {
      const testError = 'Static not working for absotule URLs';
      // Prepare environment for simulating production environment
      var env = process.env;
      process.env = {
        NODE_ENV: 'production',
        STATIC_URL: 'https://mycnd.com'
      }

      var cdnStatic = helpers({}).static('http://example.com/static.file');
      assert.equal(cdnStatic, 'http://example.com/static.file', testError);

      var cdnStatic = helpers({}).static('https://example.com/static.file');
      assert.equal(cdnStatic, 'https://example.com/static.file', testError);

      var cdnStatic = helpers({}).static('//example.com/static.file');
      assert.equal(cdnStatic, '//example.com/static.file', testError);

      process.env = env;
    });

  });

  describe('newLineToBr', function() {

    it('should replace new lines to br tag', function() {
      var response = helpers({}).newLineToBr("My message have \ntwo lines!");
      assert.equal(response, 'My message have <br />two lines!');
    })
  });

  describe('urlize', function() {

    it('should replace new lines to br tag', function() {
      var response = helpers({}).urlize("This is a site http://www.example.com/ !");
      assert.equal(response, 'This is a site <a href="http://www.example.com/" rel="nofollow" target="_blank">www.example.com/</a> !');
    })
  });

  describe('pluralize', function() {

    it('should write no results for empty list', function() {
      var response = helpers({}).pluralize([], 'No results', 'result', 'results');
      assert.equal(response, 'No results');
    });

    it('should write no results for number 0', function() {
      var response = helpers({}).pluralize(0, 'No results', 'result', 'results');
      assert.equal(response, 'No results');
    });

    it('should write 1 result for one element list', function() {
      var response = helpers({}).pluralize([1], 'No results', 'result', 'results');
      assert.equal(response, '1 result');
    });

    it('should write 1 result for number 1', function() {
      var response = helpers({}).pluralize(1, 'No results', 'result', 'results');
      assert.equal(response, '1 result');
    });

    it('should write n results for list with more than 1 element', function() {
      var response = helpers({}).pluralize([1, 2], 'No results', 'result', 'results');
      assert.equal(response, '2 results');
      var response = helpers({}).pluralize([1, 2, 3], 'No results', 'result', 'results');
      assert.equal(response, '3 results');
    });

    it('should write 2 results for number 2', function() {
      var response = helpers({}).pluralize(2, 'No results', 'result', 'results');
      assert.equal(response, '2 results');
      var response = helpers({}).pluralize(3, 'No results', 'result', 'results');
      assert.equal(response, '3 results');
    });

    it('should write no results for undefined element', function() {
      var response = helpers({}).pluralize(null, 'No results', 'result', 'results');
      assert.equal(response, 'No results');
    })
  })
});
