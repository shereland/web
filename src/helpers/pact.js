helpers = {
  logError: function(err) {
    console.error(err.response.errors);
    console.error(err.response.data);
  }
}

module.exports = helpers;
