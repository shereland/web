helpers = {
  pagination: function(currentPage, lastPage) {
    return {
      currentPage: currentPage,
      nextPage: currentPage < lastPage ? currentPage + 1 : null,
      previousPage: currentPage > 1 ? currentPage - 1 : null,
    }
  }
}

module.exports = helpers;
